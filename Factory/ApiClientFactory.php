<?php

declare(strict_types=1);


namespace Ipresso\MagentoIntegration\Factory;

use GuzzleHttp\Client;
use Ipresso\MagentoIntegration\Api\IntegrationConfigurationInterface;

class ApiClientFactory
{
    private const CONNECT_TIMEOUT = 1.0;
    private const REQUEST_TIMEOUT = 2.0;

    private ?Client $client = null;
    private IntegrationConfigurationInterface $configuration;

    public function __construct(
        IntegrationConfigurationInterface $configuration
    )
    {
        $this->configuration = $configuration;
    }

    /**
     * @return Client
     */
    public function createClient(): Client
    {
        if ($this->client) {
            return $this->client;
        }
        $this->client = new Client([
            'base_uri' => IntegrationConfigurationInterface::INTEGRATION_URL,
            'http_errors' => false,
            'verify' => false,
            'headers' => [
                'IPRESSO-API-TOKEN' => $this->configuration->getApiCredentials()
            ],
            'connect_timeout' => self::CONNECT_TIMEOUT,
            'timeout' => self::REQUEST_TIMEOUT,
        ]);
        return $this->client;
    }
}
