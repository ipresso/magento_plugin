<?php

declare(strict_types=1);


namespace Ipresso\MagentoIntegration\Factory;

use Ipresso\MagentoIntegration\Dto\ConsentDto;
use Ipresso\MagentoIntegration\Dto\InstallDto;

class DtoFactory
{
    /**
     * @param array $body
     * @return InstallDto
     */
    public static function buildInstallDto(array $body): InstallDto
    {
        return new InstallDto(
            $body['monitoringCode'] ?? '',
            $body['authToken'] ?? '',
            $body['policyPattern'] ?? ''
        );
    }
}
