<?php

declare(strict_types=1);


namespace Ipresso\MagentoIntegration\Setup;

use GuzzleHttp\Exception\GuzzleException;
use Ipresso\MagentoIntegration\Api\IntegrationConfigurationInterface;
use Ipresso\MagentoIntegration\Api\IpressoApiInterface;
use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UninstallInterface;

class Uninstall implements UninstallInterface
{
    private CustomerSetupFactory $customerSetupFactory;
    private IntegrationConfigurationInterface $configuration;
    private ModuleDataSetupInterface $moduleSetup;
    private IpressoApiInterface $ipressoApi;


    public function __construct(
        CustomerSetupFactory $customerSetupFactory,
        IntegrationConfigurationInterface $configuration,
        ModuleDataSetupInterface $moduleSetup,
        IpressoApiInterface $ipressoApi
    )
    {
        $this->customerSetupFactory = $customerSetupFactory;
        $this->configuration = $configuration;
        $this->moduleSetup = $moduleSetup;
        $this->ipressoApi = $ipressoApi;
    }

    public function uninstall(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        try {
            $this->ipressoApi->uninstall();
        } catch (GuzzleException $exception) {
            //ignore
        }
        $this->configuration->uninstall();
    }
}
