<?php

declare(strict_types=1);


namespace Ipresso\MagentoIntegration\Setup\Patch\Data;

use Ipresso\MagentoIntegration\Api\IntegrationConfigurationInterface;
use Ipresso\MagentoIntegration\Enum\IpressoAttribute;
use Magento\Customer\Model\Customer;
use Magento\Customer\Model\ResourceModel\Attribute as AttributeResource;
use Magento\Customer\Setup\CustomerSetup;
use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Eav\Model\Config;
use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Validator\ValidateException;
use Magento\Integration\Model\ConfigBasedIntegrationManager;

class InitialPatch implements DataPatchInterface
{
    protected CustomerSetupFactory $customerSetupFactory;
    private Config $eavConfig;
    private AttributeResource $attributeResource;
    private ConfigBasedIntegrationManager $integrationManager;
    private ModuleDataSetupInterface $moduleDataSetup;
    private IntegrationConfigurationInterface $configuration;

    public function __construct(
        CustomerSetupFactory $customerSetupFactory,
        Config $eavConfig,
        AttributeResource $attributeResource,
        ConfigBasedIntegrationManager $integrationManager,
        ModuleDataSetupInterface $moduleDataSetup,
        IntegrationConfigurationInterface $configuration
    )
    {
        $this->customerSetupFactory = $customerSetupFactory;
        $this->eavConfig = $eavConfig;
        $this->attributeResource = $attributeResource;
        $this->integrationManager = $integrationManager;
        $this->moduleDataSetup = $moduleDataSetup;
        $this->configuration = $configuration;
    }

    /**
     * @return array|string[]
     */
    public static function getDependencies(): array
    {
        return [];
    }

    /**
     * @return array|string[]
     */
    public function getAliases(): array
    {
        return [];
    }

    /**
     * @return void
     */
    public function apply()
    {
        $this->moduleDataSetup->getConnection()->startSetup();
        try {
            $this->install();
        } catch (LocalizedException|ValidateException $e) {
            $this->moduleDataSetup->getConnection()->rollBack();
        }
        $this->moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * @return void
     */
    public function revert()
    {
        $this->moduleDataSetup->getConnection()->startSetup();
        $this->removeAttributes();
        $this->configuration->uninstall();
        $this->moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * @throws ValidateException
     * @throws LocalizedException
     */
    private function install()
    {
        $this->integrationManager->processConfigBasedIntegrations([
            IntegrationConfigurationInterface::INTEGRATION_NAME => [
                'email' => IntegrationConfigurationInterface::INTEGRATION_EMAIL,
                'endpoint_url' => sprintf("%s/magento/api/v1/activate",
                    IntegrationConfigurationInterface::INTEGRATION_URL
                ),
                'identity_link_url' => sprintf("%s/magento/api/v1/identity",
                    IntegrationConfigurationInterface::INTEGRATION_URL
                ),
                'resource' => [
                    'Magento_Customer::customer',
                    'Magento_Customer::manage',
                    'Magento_Customer::actions',
                    'Magento_Catalog::catalog',
                    'Magento_Catalog::catalog_inventory',
                    'Magento_Catalog::products',
                    'Magento_Catalog::attributes_attributes',
                    'Magento_Sales::sales',
                    'Magento_Sales::sales_operation',
                    'Magento_Sales::sales_order',
                    'Magento_Sales::actions',
                    'Magento_Sales::actions_view',
                    'Magento_Backend::stores',
                    'Magento_Backend::stores_settings',
                    'Magento_Backend::store',
                ]
            ]
        ]);
        /** @var CustomerSetup $customerSetup */
        $customerSetup = $this->customerSetupFactory->create(['setup' => $this->moduleDataSetup]);
        $customerEntity = $customerSetup->getEavConfig()->getEntityType(Customer::ENTITY);
        $attributeSetId = $customerSetup->getDefaultAttributeSetId($customerEntity->getEntityTypeId());
        $attributeGroup = $customerSetup->getDefaultAttributeGroupId($customerEntity->getEntityTypeId(),
            $attributeSetId);
        $customerSetup->addAttribute(
            Customer::ENTITY,
            IpressoAttribute::PIXEL,
            [
                'type' => 'varchar',
                'label' => 'iPresso monitoring pixel',
                'input' => 'text',
                'required' => false,
                'visible' => false,
                'user_defined' => true,
                'position' => 999,
                'system' => false,
                'is_used_in_grid' => false,
                'is_visible_in_grid' => false,
                'is_html_allowed_on_front' => false,
                'visible_on_front' => false
            ]
        );

        $attribute = $this->eavConfig
            ->getAttribute(Customer::ENTITY, IpressoAttribute::PIXEL)
            ->addData([
                'used_in_forms' => [],
                'attribute_set_id' => $attributeSetId,
                'attribute_group_id' => $attributeGroup
            ]);
        $this->attributeResource->save($attribute);
    }

    private function removeAttributes()
    {
        /** @var CustomerSetup $customerSetup */
        $customerSetup = $this->customerSetupFactory->create(['setup' => $this->moduleDataSetup]);
        $customerSetup->removeAttribute(
            Customer::ENTITY,
            IpressoAttribute::PIXEL
        );
    }
}
