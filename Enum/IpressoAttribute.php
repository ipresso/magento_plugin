<?php

declare(strict_types=1);


namespace Ipresso\MagentoIntegration\Enum;

class IpressoAttribute
{
    public const PIXEL = 'monitoring_pixel';
    public const API_TOKEN = 'ipresso_api_token';
    public const MONITORING_CODE = 'ipresso_monitoring_code';
    public const POLICY_PATTERN = 'ipresso_policy_pattern';
    public const PRODUCT_SYNC = 'ipresso_products_synchronization';
}
