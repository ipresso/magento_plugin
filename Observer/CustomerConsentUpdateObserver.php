<?php

declare(strict_types=1);


namespace Ipresso\MagentoIntegration\Observer;

use Ipresso\MagentoIntegration\Api\IntegrationConfigurationInterface;
use Ipresso\MagentoIntegration\Api\IpressoApiInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class CustomerConsentUpdateObserver implements ObserverInterface
{
    private IpressoApiInterface $ipressoApi;
    protected CustomerRepositoryInterface $customerRepository;
    protected Context $context;
    protected IntegrationConfigurationInterface $configuration;

    public function __construct(
        CustomerRepositoryInterface $customerRepository,
        IpressoApiInterface $ipressoApi,
        Context $context,
        IntegrationConfigurationInterface $configuration
    )
    {
        $this->ipressoApi = $ipressoApi;
        $this->customerRepository = $customerRepository;
        $this->context = $context;
        $this->configuration = $configuration;
    }

    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer): void
    {
        try {
            /** @var CustomerInterface $customer */
            $customer = $observer->getData('customer');

            $this->ipressoApi->saveCustomer($customer);
        } catch (\Throwable $e) {
            //ignore
        }
    }
}
