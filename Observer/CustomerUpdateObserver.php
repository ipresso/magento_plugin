<?php

declare(strict_types=1);


namespace Ipresso\MagentoIntegration\Observer;

use Ipresso\MagentoIntegration\Api\IntegrationConfigurationInterface;
use Ipresso\MagentoIntegration\Api\IpressoApiInterface;
use Ipresso\MagentoIntegration\Traits\IpressoPixelTrait;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class CustomerUpdateObserver implements ObserverInterface
{
    use IpressoPixelTrait;

    private CustomerRepositoryInterface $customerRepository;
    private IpressoApiInterface $ipressoApi;
    private Context $context;
    private IntegrationConfigurationInterface $configuration;

    public function __construct(
        CustomerRepositoryInterface $customerRepository,
        IpressoApiInterface $ipressoApi,
        Context $context,
        IntegrationConfigurationInterface $configuration
    )
    {
        $this->customerRepository = $customerRepository;
        $this->ipressoApi = $ipressoApi;
        $this->context = $context;
        $this->configuration = $configuration;
    }

    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer): void
    {
        try {

            $email = (string)$observer->getData('email');
            $customer = $this->customerRepository->get($email);

            $response = $this->ipressoApi->saveCustomer($customer);

            $this->savePixel($response, $customer);

        } catch (\Throwable $e) {
            //ignore
        }
    }
}
