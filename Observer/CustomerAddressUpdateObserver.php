<?php

declare(strict_types=1);


namespace Ipresso\MagentoIntegration\Observer;

use Ipresso\MagentoIntegration\Api\IpressoApiInterface;
use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class CustomerAddressUpdateObserver implements ObserverInterface
{
    private IpressoApiInterface $ipressoApi;

    public function __construct(
        IpressoApiInterface $ipressoApi
    )
    {
        $this->ipressoApi = $ipressoApi;
    }

    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer): void
    {
        try {
            /** @var CustomerInterface $customer */
            $customer = $observer->getData('customer');

            $this->ipressoApi->saveCustomer($customer);
        } catch (\Throwable $e) {
            //ignore
        }
    }
}
