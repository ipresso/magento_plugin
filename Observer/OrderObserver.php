<?php

declare(strict_types=1);


namespace Ipresso\MagentoIntegration\Observer;

use Ipresso\MagentoIntegration\Api\IpressoApiInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Api\Data\OrderInterface;

class OrderObserver implements ObserverInterface
{
    private IpressoApiInterface $ipressoApi;

    public function __construct(
        IpressoApiInterface $ipressoApi
    )
    {
        $this->ipressoApi = $ipressoApi;
    }

    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer): void
    {
        try {
            /** @var OrderInterface $order */
            $order = $observer->getData('order');
            $this->ipressoApi->orderCompleteActivity($order);
        } catch (\Throwable $e) {
            //ignore
        }

    }
}
