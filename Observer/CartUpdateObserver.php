<?php

declare(strict_types=1);


namespace Ipresso\MagentoIntegration\Observer;

use Ipresso\MagentoIntegration\Api\IpressoApiInterface;
use Ipresso\MagentoIntegration\Dto\CartUpdateDto;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Checkout\Model\Cart;
use Magento\Customer\Model\Session;
use Magento\Framework\DataObject;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Psr\Log\LoggerInterface;

class CartUpdateObserver implements ObserverInterface
{
    private Session $customerSession;
    private IpressoApiInterface $ipressoApi;
    private ProductRepositoryInterface $productRepository;
    private LoggerInterface $logger;

    public function __construct(
        Session $customerSession,
        IpressoApiInterface $ipressoApi,
        ProductRepositoryInterface $productRepository,
        LoggerInterface $logger
    )
    {
        $this->customerSession = $customerSession;
        $this->ipressoApi = $ipressoApi;
        $this->productRepository = $productRepository;
        $this->logger = $logger;
    }

    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer): void
    {
        try {
            $customerId = (string)$this->customerSession->getCustomerId();
            if (!$customerId) {
                return;
            }
            /** @var Cart $cart */
            $cart = $observer->getData('cart');
            /** @var DataObject $info */
            $info = $observer->getData('info');
            $cartUpdateDto = [];

            foreach ($info->getData() ?? [] as $itemId => $itemInfo) {
                $item = $cart->getQuote()->getItemById($itemId);
                if (empty($item)) {
                    continue;
                }
                $qt = isset($itemInfo['qty']) ? (float)$itemInfo['qty'] : 0;
                $beforeQt = $item->getQty();
                if ($qt === $beforeQt) {
                    continue;
                }
                $product = $this->productRepository->getById($item->getproduct()->getId());
                $cartUpdateDto[] = new CartUpdateDto(
                    $product,
                    $qt,
                    $beforeQt
                );
            }

            if (empty($cartUpdateDto)) {
                return;
            }

            $this->ipressoApi->cartUpdateActivity(
                $customerId,
                $cartUpdateDto,
            );

        } catch (\Throwable $e) {
            $this->logger->warning($e->getMessage(), [
                'file' => $e->getFile(),
                'line' => $e->getLine()
            ]);
            //ignore
        }
    }
}
