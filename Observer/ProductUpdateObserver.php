<?php

declare(strict_types=1);


namespace Ipresso\MagentoIntegration\Observer;

use Ipresso\MagentoIntegration\Api\IntegrationConfigurationInterface;
use Ipresso\MagentoIntegration\Api\IpressoApiInterface;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Psr\Log\LoggerInterface;

class ProductUpdateObserver implements ObserverInterface
{
    private IpressoApiInterface $ipressoApi;
    private ProductRepositoryInterface $productRepository;
    private IntegrationConfigurationInterface $configuration;
    private LoggerInterface $logger;

    public function __construct(
        IpressoApiInterface $ipressoApi,
        ProductRepositoryInterface $productRepository,
        IntegrationConfigurationInterface $configuration,
        LoggerInterface $logger
    )
    {
        $this->ipressoApi = $ipressoApi;
        $this->productRepository = $productRepository;
        $this->configuration = $configuration;
        $this->logger = $logger;
    }

    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer): void
    {
        try {
            $this->logger->debug('update product event');
            if (!$this->configuration->isProductSyncEnabled()) {
                return;
            }
            /** @var ProductInterface $productEvent */
            $productEvent = $observer->getData('product');
            $product = $this->productRepository->getById($productEvent->getId());
            $this->ipressoApi->updateProduct($product);
            $this->logger->debug('update product event success');

        } catch (\Throwable $e) {
            $this->logger->warning('update product event error', [
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine()
            ]);
            //ignore
        }
    }
}
