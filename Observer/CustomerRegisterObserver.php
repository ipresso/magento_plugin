<?php

declare(strict_types=1);


namespace Ipresso\MagentoIntegration\Observer;

use Ipresso\MagentoIntegration\Api\IntegrationConfigurationInterface;
use Ipresso\MagentoIntegration\Api\IpressoApiInterface;
use Ipresso\MagentoIntegration\Traits\IpressoPixelTrait;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\Action\Context as ActionContext;

class CustomerRegisterObserver implements ObserverInterface
{
    use IpressoPixelTrait;

    private IpressoApiInterface $ipressoApi;
    protected CustomerRepositoryInterface $customerRepository;
    protected ActionContext $context;
    protected IntegrationConfigurationInterface $configuration;

    public function __construct(
        CustomerRepositoryInterface $customerRepository,
        IpressoApiInterface $ipressoApi,
        ActionContext $context,
        IntegrationConfigurationInterface $configuration
    )
    {
        $this->ipressoApi = $ipressoApi;
        $this->customerRepository = $customerRepository;
        $this->context = $context;
        $this->configuration = $configuration;
    }

    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer): void
    {
        try {

            /** @var CustomerInterface $customer */
            $customer = $observer->getData('customer');

            $response = $this->ipressoApi->saveCustomer($customer, true);

            $this->savePixel($response, $customer);

        } catch (\Throwable $e) {
            //ignore
        }
    }
}
