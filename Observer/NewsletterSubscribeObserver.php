<?php

declare(strict_types=1);


namespace Ipresso\MagentoIntegration\Observer;

use Ipresso\MagentoIntegration\Api\IpressoApiInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Newsletter\Model\Subscriber;

class NewsletterSubscribeObserver implements ObserverInterface
{
    private IpressoApiInterface $ipressoApi;

    public function __construct(
        IpressoApiInterface $ipressoApi
    )
    {
        $this->ipressoApi = $ipressoApi;
    }

    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer): void
    {
        try {
            /** @var Subscriber $subscriber */
            $subscriber = $observer->getEvent()->getSubscriber();
            $email = $subscriber->getSubscriberEmail();
            $storeId = $subscriber->getStoreId();

            $this->ipressoApi->addSubscriber($email, $storeId, $subscriber->isSubscribed());
        } catch (\Throwable $e) {
            //ignore
        }
    }
}
