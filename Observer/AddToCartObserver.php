<?php

declare(strict_types=1);


namespace Ipresso\MagentoIntegration\Observer;

use Ipresso\MagentoIntegration\Api\IpressoApiInterface;
use Magento\Catalog\Model\Product;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class AddToCartObserver implements ObserverInterface
{
    private Session $customerSession;
    private IpressoApiInterface $ipressoApi;

    public function __construct(
        Session $customerSession,
        IpressoApiInterface $ipressoApi
    )
    {
        $this->customerSession = $customerSession;
        $this->ipressoApi = $ipressoApi;
    }

    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer): void
    {
        try {
            /** @var Product $product */
            $product = $observer->getData('product');
            /** @var Http $request */
            $request = $observer->getData('request');
            $params = $request->getParams();
            $customerId = (string)$this->customerSession->getCustomerId();
            if ($customerId) {
                $this->ipressoApi->addToCartActivity(
                    $customerId,
                    $product,
                    (float)($params['qty'] ?? 1.0)
                );
            }
        } catch (\Throwable $e) {
            //ignore
        }

    }
}
