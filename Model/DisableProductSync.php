<?php

declare(strict_types=1);


namespace Ipresso\MagentoIntegration\Model;

use Ipresso\MagentoIntegration\Api\DisableProductSyncInterface;
use Ipresso\MagentoIntegration\Api\IntegrationConfigurationInterface;

class DisableProductSync implements DisableProductSyncInterface
{
    private IntegrationConfigurationInterface $configuration;

    public function __construct(
        IntegrationConfigurationInterface $configuration
    )
    {
        $this->configuration = $configuration;
    }

    /**
     * @return string
     */
    public function disableProductSync(): string
    {
        $this->configuration->setProductSync(false);
        return json_encode(['enabled' => false]);
    }
}
