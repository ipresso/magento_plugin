<?php

declare(strict_types=1);


namespace Ipresso\MagentoIntegration\Model;

use Ipresso\MagentoIntegration\Api\FinishIntegrationInstallationInterface;
use Ipresso\MagentoIntegration\Api\IntegrationConfigurationInterface;
use Ipresso\MagentoIntegration\Dto\InstallCompleteDto;
use Ipresso\MagentoIntegration\Factory\DtoFactory;
use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\App\PlainTextRequestInterface;
use Magento\Framework\App\ProductMetadataInterface;

class FinishIntegrationInstallation implements FinishIntegrationInstallationInterface
{
    private PlainTextRequestInterface $textRequest;
    private ProductMetadataInterface $productMetadata;
    private IntegrationConfigurationInterface $configuration;
    private TypeListInterface $cacheTypeList;

    public function __construct(
        PlainTextRequestInterface $textRequest,
        ProductMetadataInterface $productMetadata,
        IntegrationConfigurationInterface $configuration,
        TypeListInterface $cacheTypeList


    )
    {
        $this->textRequest = $textRequest;
        $this->productMetadata = $productMetadata;
        $this->configuration = $configuration;
        $this->cacheTypeList = $cacheTypeList;
    }

    /**
     * @inheritDoc
     */
    public function finishInstall(): array
    {
        $content = $this->textRequest->getContent();

        $body = json_decode($content, true);
        $installDto = DtoFactory::buildInstallDto($body);
        $this->configuration->saveMonitoringCode($installDto->monitoringCode);
        $this->configuration->saveApiToken($installDto->authToken);
        $this->configuration->savePolicyPattern($installDto->policyPattern);
        $types = [
            'config',
            'collections',
            'reflection',
            'eav',
            'config_integration',
            'config_integration_api',
            'config_webservice'
        ];
        foreach ($types as $type) {
            $this->cacheTypeList->cleanType($type);
        }

        $installDto = new InstallCompleteDto(
            $this->productMetadata->getVersion(),
            $this->productMetadata->getEdition(),
            IntegrationConfigurationInterface::PLUGIN_VERSION
        );
        return ['data' => json_decode(json_encode($installDto), true)];
    }
}
