<?php

declare(strict_types=1);


namespace Ipresso\MagentoIntegration\Model;

use Ipresso\MagentoIntegration\Api\EnableProductSyncInterface;
use Ipresso\MagentoIntegration\Api\IntegrationConfigurationInterface;

class EnableProductSync implements EnableProductSyncInterface
{
    private IntegrationConfigurationInterface $configuration;

    public function __construct(
        IntegrationConfigurationInterface $configuration
    )
    {
        $this->configuration = $configuration;
    }

    /**
     * @return string
     */
    public function enableProductSync(): string
    {
        $this->configuration->setProductSync(true);
        return json_encode(['enabled' => true]);
    }
}
