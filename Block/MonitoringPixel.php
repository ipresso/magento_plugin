<?php

declare(strict_types=1);


namespace Ipresso\MagentoIntegration\Block;

use Ipresso\MagentoIntegration\Enum\IpressoAttribute;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\Session;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\View\Helper\SecureHtmlRenderer;

class MonitoringPixel extends Template
{
    /**
     * @var int|null
     */
    private $currentCustomer;

    public string $pixel;
    private Session $session;
    private CustomerRepositoryInterface $customerRepository;
    private SecureHtmlRenderer $htmlRenderer;

    /**
     * @param Context $context
     * @param Session $session
     * @param CustomerRepositoryInterface $customerRepository
     * @param SecureHtmlRenderer $htmlRenderer
     */
    public function __construct(
        Context $context,
        Session $session,
        CustomerRepositoryInterface $customerRepository,
        SecureHtmlRenderer $htmlRenderer
    )
    {
        parent::__construct($context, []);
        $this->storeManager = $context->getStoreManager();
        $this->session = $session;
        $this->customerRepository = $customerRepository;
        $this->htmlRenderer = $htmlRenderer;
    }

    /**
     * @return string
     */
    public function getMonitoringPixel(): string
    {
        try {
            $customerId = $this->session->getCustomerId();
            if ($this->currentCustomer == $customerId && $this->pixel) {
                return $this->pixel;
            }
            $customer = $this->customerRepository->getById($customerId);
            $this->pixel = (string)$customer->getCustomAttribute(IpressoAttribute::PIXEL)->getValue();
            $this->currentCustomer = $customerId;
            return $this->htmlRenderer->renderTag('img', [
                'src' => $this->pixel,
                'width' => 0,
                'height' => 0,
                'alt' => "",
                'style' => "opacity: 0;"
            ]);
        } catch (\Throwable $e) {
            //ignore
        }
        return '';
    }
}
