<?php

declare(strict_types=1);


namespace Ipresso\MagentoIntegration\Block;

use Ipresso\MagentoIntegration\Api\IntegrationConfigurationInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Helper\SecureHtmlRenderer;

class HeadJs extends Template
{
    private SecureHtmlRenderer $htmlRenderer;
    private IntegrationConfigurationInterface $configuration;

    public function __construct(
        Template\Context $context,
        SecureHtmlRenderer $htmlRenderer,
        IntegrationConfigurationInterface $configuration
    )
    {
        parent::__construct($context, []);
        $this->htmlRenderer = $htmlRenderer;
        $this->configuration = $configuration;
    }

    public function getCode(): string
    {
        return $this->htmlRenderer->renderTag('script', [
            'async' => "",
            'type' => 'text/javascript'
        ], $this->configuration->getMonitoringCode(),
            false
        );
    }
}
