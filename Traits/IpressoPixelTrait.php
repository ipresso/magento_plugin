<?php

declare(strict_types=1);


namespace Ipresso\MagentoIntegration\Traits;

use Ipresso\MagentoIntegration\Enum\IpressoAttribute;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Api\Data\CustomerInterface;
use Psr\Http\Message\ResponseInterface;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\State\InputMismatchException;

/**
 * @property CustomerRepositoryInterface $customerRepository
 */
trait IpressoPixelTrait
{
    /**
     * @param ResponseInterface $response
     * @param CustomerInterface $customer
     * @throws InputException
     * @throws InputMismatchException
     * @throws LocalizedException
     */
    protected function savePixel(ResponseInterface $response, CustomerInterface $customer): void
    {
        if ($response->getStatusCode() === 200) {
            $body = json_decode($response->getBody()->getContents(), true);
            $pixel = $body['pixel'] ?? '';
            if (!empty($pixel)) {
                $customer->setCustomAttribute(IpressoAttribute::PIXEL, $pixel);
                $this->customerRepository->save($customer);
            }
        }
    }
}
