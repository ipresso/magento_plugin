<?php

declare(strict_types=1);


namespace Ipresso\MagentoIntegration\Dto;

class ConsentDto
{
    public string $content;
    public string $key;

    /**
     * @param string $key
     * @param string $content
     */
    public function __construct(
        string $key,
        string $content
    )
    {
        $this->content = $content;
        $this->key = $key;
    }
}
