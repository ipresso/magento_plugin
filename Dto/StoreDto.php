<?php

declare(strict_types=1);


namespace Ipresso\MagentoIntegration\Dto;

class StoreDto
{
    public int $storeId;
    public int $websiteId;
    public string $code;
    public string $name;

    /**
     * @param int $storeId
     * @param int $websiteId
     * @param string $code
     * @param string $name
     */
    public function __construct(
        int $storeId,
        int $websiteId,
        string $code,
        string $name
    )
    {
        $this->storeId = $storeId;
        $this->websiteId = $websiteId;
        $this->code = $code;
        $this->name = $name;
    }
}
