<?php

declare(strict_types=1);


namespace Ipresso\MagentoIntegration\Dto;

use Magento\Catalog\Api\Data\ProductInterface;

class CartUpdateDto
{
    public ProductInterface $product;
    public float $qt;
    public float $beforeQt;

    /**
     * @param ProductInterface $product
     * @param float $qt
     * @param float $beforeQt
     */
    public function __construct(
        ProductInterface $product,
        float $qt,
        float $beforeQt
    )
    {
        $this->product = $product;
        $this->qt = $qt;
        $this->beforeQt = $beforeQt;
    }
}
