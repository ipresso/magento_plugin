<?php

declare(strict_types=1);


namespace Ipresso\MagentoIntegration\Dto;

class InstallDto
{
    public string $monitoringCode;
    public string $authToken;
    public string $policyPattern;

    /**
     * @param string $monitoringCode
     * @param string $authToken
     * @param string $policyPattern
     */
    public function __construct(
        string $monitoringCode,
        string $authToken,
        string $policyPattern
    )
    {
        $this->monitoringCode = $monitoringCode;
        $this->authToken = $authToken;
        $this->policyPattern = $policyPattern;
    }
}
