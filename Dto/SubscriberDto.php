<?php

declare(strict_types=1);


namespace Ipresso\MagentoIntegration\Dto;

class SubscriberDto
{
    public string $email;
    public bool $subscribed;
    public int $storeId;

    /**
     * @param string $email
     * @param bool $subscribed
     * @param int $storeId
     */
    public function __construct(
        string $email,
        bool $subscribed,
        int $storeId
    )
    {
        $this->email = $email;
        $this->subscribed = $subscribed;
        $this->storeId = $storeId;
    }
}
