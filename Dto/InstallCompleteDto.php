<?php

declare(strict_types=1);


namespace Ipresso\MagentoIntegration\Dto;

class InstallCompleteDto
{
    public string $instanceVersion;
    public string $pluginVersion;
    public string $edition;

    /**
     * @param string $instanceVersion
     * @param string $edition
     * @param string $pluginVersion
     */
    public function __construct(
        string $instanceVersion,
        string $edition,
        string $pluginVersion
    )
    {
        $this->instanceVersion = $instanceVersion;
        $this->pluginVersion = $pluginVersion;
        $this->edition = $edition;
    }
}
