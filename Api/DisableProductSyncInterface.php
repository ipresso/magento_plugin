<?php

declare(strict_types=1);


namespace Ipresso\MagentoIntegration\Api;

interface DisableProductSyncInterface
{
    /**
     * @return string
     */
    public function disableProductSync(): string;
}
