<?php

declare(strict_types=1);


namespace Ipresso\MagentoIntegration\Api;


interface FinishIntegrationInstallationInterface
{
    /**
     * @return array
     */
    public function finishInstall(): array;
}
