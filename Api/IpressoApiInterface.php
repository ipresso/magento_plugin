<?php

declare(strict_types=1);


namespace Ipresso\MagentoIntegration\Api;

use GuzzleHttp\Exception\GuzzleException;
use Ipresso\MagentoIntegration\Dto\InstallCompleteDto;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Psr\Http\Message\ResponseInterface;

interface IpressoApiInterface
{
    /**
     * @param string $email
     * @param int $storeId
     * @param bool $consent
     * @return ResponseInterface
     * @throws GuzzleException
     */
    public function addSubscriber(string $email, int $storeId, bool $consent = true): ResponseInterface;

    /**
     * @param CustomerInterface $customer
     * @param bool $isRegister
     * @return ResponseInterface
     * @throws GuzzleException
     */
    public function saveCustomer(CustomerInterface $customer, bool $isRegister = false): ResponseInterface;

    /**
     * @param string $customerId
     * @param ProductInterface $product
     * @param float $qt
     * @return ResponseInterface
     * @throws GuzzleException
     */
    public function addToCartActivity(
        string $customerId,
        ProductInterface $product,
        float $qt = 1.0
    ): ResponseInterface;

    /**
     * @param string $customerId
     * @param array $cartUpdates
     * @return ResponseInterface
     * @throws GuzzleException
     */
    public function cartUpdateActivity(
        string $customerId,
        array $cartUpdates
    ): ResponseInterface;

    /**
     * @param int $customerId
     * @return ResponseInterface
     * @throws GuzzleException
     */
    public function loginActivity(int $customerId): ResponseInterface;

    /**
     * @param OrderInterface $order
     * @return ResponseInterface
     * @throws GuzzleException
     */
    public function orderCompleteActivity(OrderInterface $order): ResponseInterface;

    /**
     * @param InstallCompleteDto $installCompleteDto
     * @return ResponseInterface
     * @throws GuzzleException
     */
    public function install(InstallCompleteDto $installCompleteDto): ResponseInterface;

    /**
     * @return ResponseInterface
     * @throws GuzzleException
     */
    public function uninstall(): ResponseInterface;

    /**
     * @param ProductInterface $product
     * @return ResponseInterface
     * @throws GuzzleException
     */
    public function updateProduct(ProductInterface $product): ResponseInterface;
}
