<?php

declare(strict_types=1);


namespace Ipresso\MagentoIntegration\Api;

interface EnableProductSyncInterface
{
    /**
     * @return string
     */
    public function enableProductSync(): string;
}
