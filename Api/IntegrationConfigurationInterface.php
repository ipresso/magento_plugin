<?php

declare(strict_types=1);


namespace Ipresso\MagentoIntegration\Api;

interface IntegrationConfigurationInterface
{
    public const PLUGIN_VERSION = '1.0.4';
    public const INTEGRATION_NAME = 'iPresso - Marketing Automation';
    public const INTEGRATION_URL = 'https://iesb.ipresso.pl';
    public const INTEGRATION_EMAIL = 'integration@ipresso.com';

    /**
     * @return string
     */
    public function getApiCredentials(): string;

    /**
     * @param string $token
     * @return void
     */
    public function saveApiToken(string $token): void;

    /**
     * @param string $code
     * @return void
     */
    public function saveMonitoringCode(string $code): void;

    /**
     * @return string
     */
    public function getMonitoringCode(): string;

    /**
     * @return string
     */
    public function getPolicyPattern(): string;

    /**
     * @param string $pattern
     * @return void
     */
    public function savePolicyPattern(string $pattern): void;

    /**
     * @param bool $enable
     * @return void
     */
    public function setProductSync(bool $enable): void;

    /**
     * @return bool
     */
    public function isProductSyncEnabled(): bool;

    /**
     * @return void
     */
    public function uninstall(): void;
}
