<?php

declare(strict_types=1);


namespace Ipresso\MagentoIntegration\Plugin\Newsletter;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\Session;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

class NewsletterSaveInterceptor
{
    private ManagerInterface $eventManager;
    private Session $customerSession;
    private CustomerRepositoryInterface $customerRepository;

    public function __construct(
        ManagerInterface $eventManager,
        Session $customerSession,
        CustomerRepositoryInterface $customerRepository
    )
    {
        $this->eventManager = $eventManager;
        $this->customerSession = $customerSession;
        $this->customerRepository = $customerRepository;
    }

    /**
     * @param $save
     * @param $result
     * @return mixed
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function afterExecute($save, $result)
    {
        $this->eventManager->dispatch('ipresso_consent_update', [
            'customer' => $this->customerRepository->getById($this->customerSession->getCustomerId())
        ]);
        return $result;
    }
}
