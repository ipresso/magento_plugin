<?php

declare(strict_types=1);


namespace Ipresso\MagentoIntegration\Plugin\Integration;

use Ipresso\MagentoIntegration\Api\IntegrationConfigurationInterface;
use Magento\Csp\Model\Collector\CspWhitelistXmlCollector;
use Magento\Csp\Model\Policy\FetchPolicy;

class CspInterceptor
{

    private IntegrationConfigurationInterface $configuration;

    public function __construct(
        IntegrationConfigurationInterface $configuration
    )
    {
        $this->configuration = $configuration;
    }

    /**
     * @inheritDoc
     */
    public function afterCollect(CspWhitelistXmlCollector $cspWhitelistXmlCollector, $defaultPolicies = []): array
    {
        $policyPattern = $this->configuration->getPolicyPattern();
        if (empty($policyPattern)) {
            return $defaultPolicies;
        }
        $defaultPolicies[] = new FetchPolicy(
            "connect-src",
            false,
            [$policyPattern],
            [],
            false,
            false,
            false,
            [],
            [],
            false,
            false
        );
        $defaultPolicies[] = new FetchPolicy(
            "script-src",
            false,
            [$policyPattern],
            [],
            false,
            false,
            false,
            [],
            [],
            false,
            false
        );
        $defaultPolicies[] = new FetchPolicy(
            'default-src',
            false,
            [$policyPattern],
            [],
            false,
            false,
            false,
            [],
            [],
            false,
            false
        );
        $defaultPolicies[] = new FetchPolicy(
            'img-src',
            false,
            [$policyPattern],
            [],
            false,
            false,
            false,
            [],
            [],
            false,
            false
        );
        $defaultPolicies[] = new FetchPolicy(
            'style-src',
            false,
            [$policyPattern],
            [],
            false,
            false,
            false,
            [],
            [],
            false,
            false
        );
        $defaultPolicies[] = new FetchPolicy(
            'media-src',
            false,
            [$policyPattern],
            [],
            false,
            false,
            false,
            [],
            [],
            false,
            false
        );
        return $defaultPolicies;
    }
}
