<?php

declare(strict_types=1);


namespace Ipresso\MagentoIntegration\Plugin\Customer;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

class AddressUpdateInterceptor
{
    private ManagerInterface $eventManager;
    private Session $customerSession;
    private CustomerRepositoryInterface $customerRepository;
    private Context $context;

    public function __construct(
        ManagerInterface $eventManager,
        Session $customerSession,
        CustomerRepositoryInterface $customerRepository,
        Context $context
    )
    {
        $this->eventManager = $eventManager;
        $this->customerSession = $customerSession;
        $this->customerRepository = $customerRepository;
        $this->context = $context;
    }

    /**
     * @param $save
     * @param $result
     * @return mixed
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function afterExecute($save, $result)
    {
        if (empty($this->context->getMessageManager()->getMessages()->getErrors())) {
            $this->eventManager->dispatch('ipresso_address_update', [
                'customer' => $this->customerRepository->getById($this->customerSession->getCustomerId())
            ]);
        }
        return $result;
    }
}
