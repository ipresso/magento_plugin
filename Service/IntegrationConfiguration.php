<?php

declare(strict_types=1);


namespace Ipresso\MagentoIntegration\Service;

use Ipresso\MagentoIntegration\Api\IntegrationConfigurationInterface;
use Ipresso\MagentoIntegration\Enum\IpressoAttribute;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;

class IntegrationConfiguration implements IntegrationConfigurationInterface
{
    private WriterInterface $writer;
    private ScopeConfigInterface $config;

    public function __construct(
        WriterInterface $writer,
        ScopeConfigInterface $config
    )
    {
        $this->writer = $writer;
        $this->config = $config;
    }

    /**
     * @ingeritdoc
     */
    public function getApiCredentials(): string
    {
        return $this->config->getValue(IpressoAttribute::API_TOKEN) ?? '';
    }

    /**
     * @ingeritdoc
     */
    public function saveApiToken(string $token): void
    {
        $this->writer->save(IpressoAttribute::API_TOKEN, $token);
    }

    /**
     * @ingeritdoc
     */
    public function saveMonitoringCode(string $code): void
    {
        $this->writer->save(IpressoAttribute::MONITORING_CODE, $code);
    }

    /**
     * @ingeritdoc
     */
    public function getMonitoringCode(): string
    {
        return $this->config->getValue(IpressoAttribute::MONITORING_CODE) ?? '';
    }

    public function getPolicyPattern(): string
    {
        return $this->config->getValue(IpressoAttribute::POLICY_PATTERN) ?? '';
    }

    public function savePolicyPattern(string $pattern): void
    {
        $this->writer->save(IpressoAttribute::POLICY_PATTERN, $pattern);
    }

    /**
     * @ingeritdoc
     */
    public function setProductSync(bool $enable): void
    {
        $this->writer->save(IpressoAttribute::PRODUCT_SYNC, (int)$enable);
    }

    /**
     * @ingeritdoc
     */
    public function isProductSyncEnabled(): bool
    {
        return (bool)$this->config->getValue(IpressoAttribute::PRODUCT_SYNC);
    }

    /**
     * @ingeritdoc
     */
    public function uninstall(): void
    {
        $this->writer->save(IpressoAttribute::MONITORING_CODE, '');
        $this->writer->save(IpressoAttribute::POLICY_PATTERN, '');
        $this->writer->save(IpressoAttribute::PRODUCT_SYNC, 0);
        $this->writer->save(IpressoAttribute::API_TOKEN, '');
    }
}
