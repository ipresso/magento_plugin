<?php

declare(strict_types=1);


namespace Ipresso\MagentoIntegration\Service;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Ipresso\MagentoIntegration\Api\IpressoApiInterface;
use Ipresso\MagentoIntegration\Dto\CartUpdateDto;
use Ipresso\MagentoIntegration\Dto\InstallCompleteDto;
use Ipresso\MagentoIntegration\Dto\SubscriberDto;
use Ipresso\MagentoIntegration\Factory\ApiClientFactory;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Psr\Http\Message\ResponseInterface;

class IpressoApi implements IpressoApiInterface
{
    private ApiClientFactory $apiFactory;

    public function __construct(
        ApiClientFactory $apiFactory
    )
    {
        $this->apiFactory = $apiFactory;
    }

    /**
     * @inheritDoc
     */
    public function addSubscriber(string $email, int $storeId, bool $consent = true): ResponseInterface
    {
        $subscriberDto = new SubscriberDto($email, $consent, $storeId);
        return $this->request('/magento/api/v1/events/add-subscriber', $subscriberDto);
    }

    /**
     * @param CustomerInterface $customer
     * @param bool $isRegister
     * @inheritDoc
     */
    public function saveCustomer(CustomerInterface $customer, bool $isRegister = false): ResponseInterface
    {
        $query = [
            'register' => $isRegister
        ];
        return $this->request('/magento/api/v1/events/customer', $customer->__toArray(), 'POST', $query);
    }

    /**
     * @inheritDoc
     */
    public function addToCartActivity(string $customerId, ProductInterface $product, float $qt = 1.0): ResponseInterface
    {
        return $this->request(
            "/magento/api/v1/events/add-to-card/$customerId",
            $product->__toArray(),
            'POST',
            [
                'qt' => $qt
            ]
        );
    }

    /**
     * @inheritDoc
     */
    public function cartUpdateActivity(
        string $customerId,
        array $cartUpdates
    ): ResponseInterface
    {
        $body = array_map(function (CartUpdateDto $dto) {
            return [
                'product' => $dto->product->__toArray(),
                'qt' => $dto->qt,
                'qbBefore' => $dto->beforeQt
            ];
        }, $cartUpdates);
        return $this->request(
            "/magento/api/v1/events/cart-update/$customerId",
            $body
        );
    }

    /**
     * @ingeritdoc
     */
    public function loginActivity(int $customerId): ResponseInterface
    {
        return $this->request("/magento/api/v1/events/login-activity/$customerId");
    }

    /**
     * @inheritDoc
     */
    public function orderCompleteActivity(OrderInterface $order): ResponseInterface
    {
        return $this->request("/magento/api/v1/events/order-complete", $order->toArray());
    }

    /**
     * @inheritDoc
     */
    public function install(InstallCompleteDto $installCompleteDto): ResponseInterface
    {
        return $this->request("/magento/api/v1/install", $installCompleteDto, 'POST', [], [
            'timeout' => 20.0,
            'connect_timeout' => 5.0
        ]);
    }

    public function uninstall(): ResponseInterface
    {
        return $this->request("/magento/api/v1/uninstall");
    }

    /**
     * @inheritDoc
     */
    public function updateProduct(ProductInterface $product): ResponseInterface
    {
        return $this->request("/magento/api/v1/events/product", $product->__toArray(), 'PUT');

    }

    /**
     * @throws GuzzleException
     */
    private function request(
        string $uri,
        $body = [],
        string $method = 'POST',
        array $query = [],
        array $options = []
    ): ResponseInterface
    {
        return $this->client()->request($method, $uri, array_merge($options, [
            'body' => json_encode($body),
            'query' => $query
        ]));
    }

    /**
     * @return Client
     */
    private function client(): Client
    {
        return $this->apiFactory->createClient();
    }
}
